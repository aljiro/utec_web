<?php
error_reporting(0);
$dir_pages = "pages/";

$page = $_GET['page'];

switch($page)
{
	case "register":
		$header .= "Register | Uncle Tom English Center (UTEC)";
		$reg_nav = 'class="active"';
		$page = $dir_pages . "register.php";
		break;

	case "about":
		$header .= "About Us | Uncle Tom English Center (UTEC)";
		$about_nav = 'class="active"';
		$page = $dir_pages . "about-us.php";
		break;

	case "product":
		$header .= "Product | Uncle Tom English Center (UTEC)";
		$product_nav = 'class="active"';
		$page = $dir_pages . "product.php";
		break;

	case "contact":
		$header .= "Contact Us | Uncle Tom English Center (UTEC)";
		$contact_nav = 'class="active"';
		$page = $dir_pages . "contact.php";
		break;

	case "yle":
		$header .= "Young Learning English | Uncle Tom English Center (UTEC)";
		$yle_nav = 'class="active"';
		$drop_nav = 'class="active"';
		$page = $dir_pages . "product/yle/yle.php";
		break;

	case "ylestarters":
		$header .= "Young Learning English - Starters | Uncle Tom English Center (UTEC)";
		$yle_nav = 'class="active"';
		$page = $dir_pages . "product/yle/yle-starters.php";
		break;

	case "ylemovers":
		$header .= "Young Learning English - Movers | Uncle Tom English Center (UTEC)";
		$yle_nav = 'class="active"';
		$page = $dir_pages . "product/yle/yle-movers.php";
		break;

	case "yleflyers":
		$header .= "Young Learning English - Flyers | Uncle Tom English Center (UTEC)";
		$yle_nav = 'class="active"';
		$page = $dir_pages . "product/yle/yle-flyers.php";
		break;

	case "ge":
		$header .= "General English for Communication | Uncle Tom English Center (UTEC)";
		$ge_nav = 'class="active"';
		$drop_nav = 'class="active"';
		$page = $dir_pages . "product/ge/ge.php";
		break;

	case "gekey":
		$header .= "General English Key (KET) | Uncle Tom English Center (UTEC)";
		$ge_nav = 'class="active"';
		$page = $dir_pages . "product/ge/ge-key.php";
		break;

	case "gepre":
		$header .= "General English Preliminary (PET) | Uncle Tom English Center (UTEC)";
		$ge_nav = 'class="active"';
		$page = $dir_pages . "product/ge/ge-pre.php";
		break;

	case "gefirst":
		$header .= "General English First (FCE) | Uncle Tom English Center (UTEC)";
		$ge_nav = 'class="active"';
		$page = $dir_pages . "product/ge/ge-first.php";
		break;

	case "geadvanced":
		$header .= "General English Advanced (CAE) | Uncle Tom English Center (UTEC)";
		$ge_nav = 'class="active"';
		$page = $dir_pages . "product/ge/ge-advanced.php";
		break;

	case "gepro":
		$header .= "General English Proficiency (CPE) | Uncle Tom English Center (UTEC)";
		$ge_nav = 'class="active"';
		$page = $dir_pages . "product/ge/ge-proficiency.php";
		break;

	case "efsp":
		$header .= "English for Spesific Purposes | Uncle Tom English Center (UTEC)";
		$efsp_nav = 'class="active"';
		$drop_nav = 'class="active"';
		$page = $dir_pages . "product/efsp/efsp.php";
		break;

	case "eappre":
		$header .= "English for Spesific Purposes | Uncle Tom English Center (UTEC)";
		$efsp_nav = 'class="active"';
		$page = $dir_pages . "product/efsp/be-pre.php";
		break;

	case "eapvan":
		$header .= "English for Spesific Purposes | Uncle Tom English Center (UTEC)";
		$efsp_nav = 'class="active"';
		$page = $dir_pages . "product/efsp/be-van.php";
		break;

	case "eaphigh":
		$header .= "English for Spesific Purposes | Uncle Tom English Center (UTEC)";
		$efsp_nav = 'class="active"';
		$page = $dir_pages . "product/efsp/be-high.php";
		break;

	case "efap":
		$header .= "English for Academic Purposes | Uncle Tom English Center (UTEC)";
		$efap_nav = 'class="active"';
		$drop_nav = 'class="active"';
		$page = $dir_pages . "product/efap/efap.php";
		break;

	case "bulats":
		$header .= "English for Academic Purposes | Uncle Tom English Center (UTEC)";
		$efap_nav = 'class="active"';
		$page = $dir_pages . "product/efap/bulats.php";
		break;

	case "ielts":
		$header .= "English for Academic Purposes | Uncle Tom English Center (UTEC)";
		$efap_nav = 'class="active"';
		$page = $dir_pages . "product/efap/ielts.php";
		break;

	case "toefl":
		$header .= "English for Academic Purposes | Uncle Tom English Center (UTEC)";
		$efap_nav = 'class="active"';
		$page = $dir_pages . "product/efap/toefl.php";
		break;

	case "toeic":
		$header .= "English for Academic Purposes | Uncle Tom English Center (UTEC)";
		$efap_nav = 'class="active"';
		$page = $dir_pages . "product/efap/toeic.php";
		break;

	case "simpanbukutamu":
		$page = $dir_pages . "simpan_bukutamu.php";
		break;

	default:
		if($page == ''){
			$header .= "Home | Uncle Tom English Center (UTEC)";
			$home_nav = 'class="active"';
			$page = $dir_pages . "home.php";
			break;
		}else{
			$header .= "Not Found | Uncle Tom English Center (UTEC)";
			$page = $dir_pages . "404.php";
			break;
		}
}

include($dir_pages . "template.php");
?>