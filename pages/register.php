	<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="#">Register</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
	<section id="about-us">
    <div class="container wow fadeInDown">
		  <div class="row">
			 <div class="col-sm-12">

        <div class="tab-wrap"> 
          <div class="media">
              <div class="parrent pull-left">
                  <ul class="nav nav-tabs nav-stacked">
                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Data Calon Siswa</a></li>
                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">Data Orang Tua</a></li>
                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Data Sekolah /  Kampus</a></li>
                  </ul>
              </div>

              <div class="parrent media-body">
                <div class="tab-content">
                  <div class="tab-pane fade active in" id="tab1">
                    <div class="media">
                      <div class="media-body">
                        <form class="formDataCalonSiswa" action="?page=simpan-pmb" method="POST">
                          <div class="form-group">
                            <input type="text" name="nama_siswa" class="form-control" placeholder="isi dengan nama calon siswa *" required>
                          </div>
                          <div class="form-group">
                            <input type="text" name="tempat_lahir" class="form-control" placeholder="isi dengan tempat lahir calon siswa *" required>
                          </div>
                          <div class="form-group">
                            <input type="text" name="tgl_lahir" class="form-control date-picker" data-date-format="yyyy-mm-dd" placeholder="isi dengan tanggal lahir calon siswa (format yyyy-mm-dd) *" required>
                          </div>
                          <div class="form-group">
                            <select id="form-field-select-1" class="form-control" name="kd_jk" required="">
                              <option value="">Pilih Jenis Kelamin ...</option>
                              <option value="P">PEREMPUAN</option>
                              <option value="L">LAKI-LAKI</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="control-label" for="form-field-1">Alamat</label>
                            <textarea class="form-control form-control-line valid" rows="8" name="alamat" style="text-transform: capitalize">Jalan : 

Kelurahan : 

Kecamatan : 

Kode Pos : </textarea>
                          </div>
                          <div class="form-group">
                            <input type="number" name="no_tlp" class="form-control" placeholder="isi dengan nomor telpon rumah calon siswa (jika ada)">
                          </div>
                          <div class="form-group">
                            <input type="number" name="np_hp" class="form-control" placeholder="isi dengan nomor hp calon siswa atau nomor hp orang tua calon siswa *" required>
                          </div>
                          <div class="form-group">
                            <input type="email" name="np_hp" class="form-control" placeholder="isi dengan email calon siswa atau email orang tua calon siswa *" required>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>

                   <div class="tab-pane fade" id="tab2">
                      <div class="tab-content">
                        <div class="media">
                          
                        </div>
                      </div>
                   </div>

                   <div class="tab-pane fade" id="tab3">
                      <div class="form-group">
                        <input type="text" class="form-control" id="form-field-1" name="nm_sekolah" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="isi dengan nama sekolah calon siswa *" required>
                      </div>

                      <div class="form-group">
                        <input type="number" class="form-control" id="form-field-1" name="tlp_sekolah" placeholder="isi dengan nomor telpon sekolah / kampus calon siswa"  required>
                      </div>

                      <div class="form-group">
                        <input type="number" class="form-control" id="form-field-1" name="kelas" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="isi dengan kelas berapa jika calon siswa masih sekolah *" required>
                      </div>

                      <div class="form-group">
                        <label class="control-label" for="form-field-1">Semester *</label>
                        <input type="text" class="form-control" id="form-field-1" name="semester" onkeyup="javascript:this.value=this.value.toUpperCase();" required>
                      </div>

                      <div class="form-group">
                        <label class="control-label" for="form-field-1">Fakultas *</label>
                        <div class="controls">
                          <input type="text" class="form-control" id="form-field-1" name="fakultas" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="isi kolom ini jika kuliah" required>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label" for="form-field-1">Jurusan *</label>
                        <div class="controls">
                          <input type="text" class="form-control" id="form-field-1" name="jurusan" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="isi kolom ini jika kuliah" required>
                        </div>
                      </div>
                        </div>
                        
                      </div>
                   </div>

                </div> <!--/.tab-content-->  
              </div> <!--/.media-body--> 
            </div> <!--/.media-->     
        </div><!--/.tab-wrap-->
      
        <div class="form-actions">
          <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            Daftar <sup style="">pastikan semua kolom diatas terisi</sup>
          </button>
          &nbsp; &nbsp; &nbsp;
          <a href="#" class="btn" type="reset">
            <i class="icon-undo bigger-110"></i>
            Batal
          </a>
        </div>
      </form>

      </div>
			</div>
		</div>
	</section><!--/about-us-->
  