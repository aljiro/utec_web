<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="#">Product</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="feature" >
        <div class="container">
            <div class="row">

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/kids.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Young Learner English</h3>
                            <p><a href="yle">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/general.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">General English for Communication</h3>
                            <p><a href="ge">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/spesific.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">English for Spesific Purposes</h3>
                            <p><a href="efsp">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/academic.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">English for Academic Purposes</h3>
                            <p><a href="efap">know more ...</a></p>
                        </div>
                    </div>
                </div>  

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/teacher.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Teacher Training Program</h3>
                            <p><a href="">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/test.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">English Test Services&nbsp;&nbsp;&nbsp;</h3>
                            <p><a href="">know more ...</a></p>
                        </div>
                    </div>
                </div>

            </div><!--/.row--> 
        </div><!--/.container-->
    </section><!--/#feature-->