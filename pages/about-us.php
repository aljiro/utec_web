	<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="#">About Us</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
	<section id="about-us">
        <div class="container wow fadeInDown">
  			<div class="row">
  				<div class="col-md-6">
  					<p class="lead">Uncle Tom English Center (UTEC) is an English Language School, established on 2005. In the course of its business operation, UTEC has been relied to hold English Education, Training and Development Programs for schools, companies, non-profit organizations to government bodies. UTEC has served for more than</p>
  					<p class="lead">
  						UTEC offers English training for you who come from all backgrounds and walks of life but have a common desire to be able to communicate in English. Here, you are prepared for communicative abilities and for other different purposes, like; socialization, business, attending lectures, conferences, etc. For these purposes, training programs with many choices to suit your needs are made available.
  					</p>
  					<p class="lead">
  						One of our missions is to be committed to give satisfaction constantly to you and users of UTEC graduates by way of good quality service, effective and efficient methods supported by highly dedicated teachers and staff.
  					</p>
  					<p class="lead">
  						We serve you better programs for In-class Training, In-house Training, In-company Training, and In-school Training.
  					</p>
  				</div>

				<div class="col-md-6">
					<div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">OUR VISION</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">OUR MISSIONS</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">OUR VALUES</a></li>
                                    <li class=""><a href="#tab4" data-toggle="tab" class="tehnical">OUR MOTTO</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p>A Trendsetter of English Center in Indonesia in 2020</p>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p>
                                                <ul>
                                                	<li>
                                                		Providing effective, efficient, fun learning and teaching with the highest level ofInternational excellence standard system
                                                	</li>
                                                	<li>
                                                		Creating, developing and empowering quality services from time to time for CUSTOMERS’ SATISFACTION and DELIGHT
                                                	</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="tab-pane fade" id="tab3">
                                        <p>
                                        	<ul>
                                        		<li>
                                        			Having strong belief to lead, influence and transform global English language educationby providing an integrated and holistic approach to learning and teaching
                                        		</li>
                                        		<li>
                                        			Having strong belief in the learning ethos and the integrated approach to education
                                        		</li>
                                        		<li>
                                        			Underpinning four key principles: Expertise, Experience, Quality and Innovation
                                        		</li>
                                        	</ul>
                                        </p>
                                     </div>
                                     
                                    <div class="tab-pane fade" id="tab4">
                                    	<p><b><i>"Say to the world"</i></b></p>
                                    </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
				</div>

				<div class="col-md-6">
					<div id="about-slider">
						<div id="carousel-slider" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
						  	<ol class="carousel-indicators visible-xs">
							    <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
							    <li data-target="#carousel-slider" data-slide-to="1"></li>
							    <li data-target="#carousel-slider" data-slide-to="2"></li>
						  	</ol>

							<div class="carousel-inner">
								<div class="item active">
									<img src="images/slider_one.jpg" class="img-responsive" alt=""> 
							   </div>
							   <div class="item">
									<img src="images/slider_one.jpg" class="img-responsive" alt=""> 
							   </div> 
							   <div class="item">
									<img src="images/slider_one.jpg" class="img-responsive" alt=""> 
							   </div> 
							</div>
						</div> <!--/#carousel-slider-->
					</div><!--/#about-slider-->
				</div>
			</div>
		</section><!--/about-us-->


		<section id="feature">
			<div class="container wow fadeInDown">
				<div class="row">
					<div class="col-sm-12">
                    	<div class="accordion">
                    		<div class="center">
                    			<h2>Why Us?</h2>
                    		</div>
                        	<div class="panel-group" id="accordion1">
                          		<div class="panel panel-default">
                            		<div class="panel-heading active">
                              			<h3 class="panel-title">
                                			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
                                  			Give learners the best support
                                  			<i class="fa fa-angle-right pull-right"></i>
                                			</a>
                              			</h3>
                            		</div>

	                            	<div id="collapseOne1" class="panel-collapse collapse in">
	                              		<div class="panel-body">
	                                  		<div class="media accordion-inner">
	                                        	<div class="pull-left">
	                                            	<img class="img-responsive" src="images/accordion1.png">
	                                        	</div>
	                                        	<div class="media-body">
	                                             	<p>Uncle Tom English Center (UTEC) has had many experiences and been relied to hold English Training Programs to meet and fit the learners needs from individuals, schools, companies, non-profit organizations to government bodies. UTEC, as a School Provider of Cambridge English Language Assessment, is committed to excellence in education and we are doing our best for quality materials that really work to learners. We have recently extended the range of innovative and flexible learning solutions including online, mobile and blended learning materials to help them achieve their goals.</p>
	                                        	</div>
	                                  		</div>
	                              		</div>
	                           		</div>
	                          	</div>

                          		<div class="panel panel-default">
                            		<div class="panel-heading">
                              			<h3 class="panel-title">
                               				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1">
                                  			Prepare for exam success
                                  			<i class="fa fa-angle-right pull-right"></i>
                                			</a>
                              			</h3>
                            		</div>
                            		<div id="collapseTwo1" class="panel-collapse collapse">
                            			<div class="panel-body">
	                                  		<div class="media accordion-inner">
	                                        	<div class="pull-left">
	                                            	<img class="img-responsive" src="images/accordion1.png">
	                                        	</div>
	                                        	<div class="media-body">
	                                             	<p>Our extensive selection of courses, supplementary materials and practice tests are developed in close collaboration with <b>Cambridge English Language Assessment</b>, the people who set the exams. So, we can prepare learners for exam success with confidence. We can be sure the language we are teaching is natural, up to date and genuinely useful for them. We use the latest research to understand how English works and to help them learn more effectively.</p>
	                                        	</div>
	                                  		</div>
	                              		</div>
                            		</div>
                          		</div>

                          		<div class="panel panel-default">
                            		<div class="panel-heading">
                              			<h3 class="panel-title">
                                			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree1">
                                  			Better results, quicker
                                  			<i class="fa fa-angle-right pull-right"></i>
                                			</a>
                              			</h3>
                            		</div>
                            		<div id="collapseThree1" class="panel-collapse collapse">
                            			<div class="panel-body">
	                                  		<div class="media accordion-inner">
	                                        	<div class="pull-left">
	                                            	<img class="img-responsive" src="images/accordion1.png">
	                                        	</div>
	                                        	<div class="media-body">
	                                             	<p>Whether preparing for an exam, an important business meeting or simply learning English for fun, we know learners learn it. So, with <b>UTEC - Cambridge English</b>, we can focus on the language they need to succeed in English fast.</p>
	                                        	</div>
	                                  		</div>
	                              		</div>
                            		</div>
                          		</div>

                          		<div class="panel panel-default">
                            		<div class="panel-heading">
                              			<h3 class="panel-title">
                                			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour1">
                                  			Understand where they need most help
                                  			<i class="fa fa-angle-right pull-right"></i>
                                			</a>
                              			</h3>
                            		</div>
                            		<div id="collapseFour1" class="panel-collapse collapse">
                            			<div class="panel-body">
	                                  		<div class="media accordion-inner">
	                                        	<div class="pull-left">
	                                            	<img class="img-responsive" src="images/accordion1.png">
	                                        	</div>
	                                        	<div class="media-body">
	                                             	<p><b>UTEC - Cambridge English</b> materials focus on common problem areas and train learners to avoid mistakes. The <b>Cambridge Learner Corpus</b> is a unique bank of exam candidate papers and the world’s largest learner language database. We use this to identify exactly what learners in each country and at each level find most difficult.</p>
	                                        	</div>
	                                  		</div>
	                              		</div>
                            		</div>
                          		</div>

                          		<div class="panel panel-default">
                            		<div class="panel-heading">
                              			<h3 class="panel-title">
                                			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive1">
                                  			Focus on the right language for their level
                                  			<i class="fa fa-angle-right pull-right"></i>
                                			</a>
                              			</h3>
                            		</div>
                            		<div id="collapseFive1" class="panel-collapse collapse">
                            			<div class="panel-body">
	                                  		<div class="media accordion-inner">
	                                        	<div class="pull-left">
	                                            	<img class="img-responsive" src="images/accordion1.png">
	                                        	</div>
	                                        	<div class="media-body">
	                                             	<p><b>The Common European Framework of Reference (CEFR)</b> is used the world-over to describe language proficiency. <b>Cambridge English</b>, as a leading member of English Profile – a council of Europe endorsed programme to describe the CEFR for English. English Profile helps us know what to teach at each level, meaning that Cambridge English materials are better aligned to the CEFR and learners can focus on the language they need to progress.</p>
	                                        	</div>
	                                  		</div>
	                              		</div>
                            		</div>
                          		</div>

                          		<div class="panel panel-default">
                            		<div class="panel-heading">
                              			<h3 class="panel-title">
                                			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseSix1">
                                  			Be aligned with international standards
                                  			<i class="fa fa-angle-right pull-right"></i>
                                			</a>
                              			</h3>
                            		</div>
                            		<div id="collapseSix1" class="panel-collapse collapse">
                            			<div class="panel-body">
	                                  		<div class="media accordion-inner">
	                                        	<div class="pull-left">
	                                            	<img class="img-responsive" src="images/accordion1.png">
	                                        	</div>
	                                        	<div class="media-body">
	                                             	<p>The <b>Cambridge English Placement Test</b> is a fast, accurate way of placing learners on English language courses.
	                                             	<ul>
	                                             		<li>
	                                             			Find out what level of English learners already have
	                                             		</li>
	                                             		<li>
	                                             			Decide which language class is most appropriate for them
	                                             		</li>
	                                             		<li>
	                                             			Make decisions about Cambridge English exam they should aim for
	                                             		</li>
	                                             	</ul>
	                                             	</p>
	                                        	</div>
	                                  		</div>
	                              		</div>
                            		</div>
                          		</div>

                        	</div><!--/#accordion1-->
                    	</div>
                	</div>
               	</div>
			</div>
		</section>
			
		<section id="about-us">
			<div class="container">
			<div class="team">
				<div class="center wow fadeInDown">
					<h2>Team of UTEC</h2>
				</div>

				<div class="row clearfix">
					<div class="col-md-4 col-sm-6">	
						<div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/tom.png" alt=""></a>
								</div>
								<div class="media-body">
									<h4>Thomas Pakaya</h4>
									<h5>Founder and CEO</h5>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Web</a></li>
										<li class="btn"><a href="#">Ui</a></li>
										<li class="btn"><a href="#">Ux</a></li>
										<li class="btn"><a href="#">Photoshop</a></li>
									</ul>
									
									<ul class="social_icons">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						</div>
					</div><!--/.col-lg-4 -->
					
					
					<div class="col-md-4 col-sm-6 col-md-offset-2">	
						<div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/meliyani.png" alt=""></a>
								</div>
								<div class="media-body">
									<h4>Meliyani</h4>
									<h5>Founder and CEO</h5>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Web</a></li>
										<li class="btn"><a href="#">Ui</a></li>
										<li class="btn"><a href="#">Ux</a></li>
										<li class="btn"><a href="#">Photoshop</a></li>
									</ul>
									<ul class="social_icons">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						</div>
					</div><!--/.col-lg-4 -->					
				</div> <!--/.row -->
				<div class="row team-bar">
					<div class="first-one-arrow hidden-xs">
						<hr>
					</div>
					<div class="first-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
					</div>
					<div class="second-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
					</div>
					<div class="third-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
					</div>
					<div class="fourth-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
					</div>
				</div> <!--skill_border-->       

				<div class="row clearfix">   
					<div class="col-md-4 col-sm-6 col-md-offset-2">	
						<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/man3.jpg" alt=""></a>
								</div>

								<div class="media-body">
									<h4>Jhon Doe</h4>
									<h5>Founder and CEO</h5>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Web</a></li>
										<li class="btn"><a href="#">Ui</a></li>
										<li class="btn"><a href="#">Ux</a></li>
										<li class="btn"><a href="#">Photoshop</a></li>
									</ul>
									<ul class="social_icons">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-md-offset-2">
						<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/man4.jpg" alt=""></a>
								</div>
								<div class="media-body">
									<h4>Jhon Doe</h4>
									<h5>Founder and CEO</h5>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Web</a></li>
										<li class="btn"><a href="#">Ui</a></li>
										<li class="btn"><a href="#">Ux</a></li>
										<li class="btn"><a href="#">Photoshop</a></li>
									</ul>
									<ul class="social_icons">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						</div>
					</div>
				</div>	<!--/.row-->
			</div><!--section-->
			</div>
		</section>
    