<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo $header; ?></title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body class="homepage">

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="top-number"><p><i class="fa fa-phone-square"></i>&nbsp;(021) 888 1919 &nbsp;&nbsp; <i class="fa fa-envelope"></i>&nbsp;info@utec.co.id</p></div>
                    </div>
                    <div class="col-sm-6">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="./"><img src="images/logo-utec.png" alt="UTEC"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li <?php echo $home_nav; ?>><a href="./">Home</a></li>
                        <li <?php echo $about_nav; ?>><a href="about">About Us</a></li>
                        <li <?php echo $drop_nav; ?> class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Product <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li <?php echo $yle_nav; ?>><a href="yle" title="Young Learner English">Young Learner ...</a></li>
                                <li <?php echo $ge_nav; ?>><a href="ge" title="General English fo Communication">General English ...</a></li>
                                <li <?php echo $efsp_nav; ?>><a href="efsp" title="English for Spesific Purposes">English For Spesific...</a></li>
                                <li <?php echo $efap_nav; ?>><a href="efap" title="English for Academic Purposes">English for Academic ...</a></li>
                                <li><a href="ttp" title="Teacher Training Program">Teacher Training ...</a></li>
                                <li><a href="ets" title="English Test Service">English Test ...</a></li>
                            </ul>
                        </li>
                        <li><a href="blog">Blog</a></li> 
                        <li <?php echo $contact_nav; ?>><a href="contact">Contact</a></li>
                        <li <?php echo $reg_nav; ?>><a href="register">Register</a></li>               
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->

    <?php 
        include $page;
    ?>

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2016 <a target="_blank" href="http://utec.co.id/" title="Uncle Tom English Center">Uncle Tom English Center</a> (UTEC). All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="./">Home</a></li>
                        <li><a href="about">About Us</a></li>
                        <li><a href="product">Product</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('.date-picker').datepicker().next().on(ace.click_event, function(){
                $(this).prev().focus();
            });
        });
    </script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
</body>
</html>