<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="ge">General English</a> / <a href="#">Key (KET)</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="other">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <img src="images/services/ge/ket.png">
                    <p class="lead"><b><u>A great first step in learning English</u></b><br><i>General English – Key</i>, also known as Key English Test (KET), is a basic level qualification that shows you can use English to communicate in simple situations. It shows you have made a good start in learning English.</p><p>&nbsp;</p>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Why Take the course?</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">What level is <i>GE – Key (KET)</i>?</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Why take the test?</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Prove you can communicate at a basic level in English</u></b></p>
                                                <p><i>GE – Key (KET)</i> is a basic level qualification that shows you can use English to communicate in simple situations</p>
                                                <p>You can use it to:
                                                <ul>
                                                    <li><b>understand</b> simple written English</li>
                                                    <li><b>communicate</b> in familiar situations</li>
                                                    <li><b>understand</b> short notices and simple spoken directions</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><i>GE – Key (KET)</i> is set at Level A2 of the Common European Framework of Reference for Languages (CEFR)</p>
                                                <p>Reaching this level means you can:
                                                <ul>
                                                    <li><b>understand and use basic phrases and expressions</b></li>
                                                    <li><b>introduce yourself and answer basic questions</b> about your personal details</li>
                                                    <li><b>interact with English speakers</b> who talk slowly and clearly</li>
                                                    <li><b>write short, simple notes</b></li>
                                                </ul>
                                                Preparing for <i>GE – Key (KET)</i> will give you these kinds of practical language skills
                                                </p>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="tab-pane fade" id="tab3">
                                        <p><b><u>Take a high-quality test that is fair to everyone</u></b></p>
                                        <p><i>GE – Key (KET)</i> is respected and accepted internationally it:
                                            <ul>
                                                <li>provides the <b>most reliable</b> reflection of your language skills</li>
                                                <li>covers <b>all major varieties of English</b> (e.g. British English, American English)</li>
                                                <li>is <b>designed to be fair</b> to users of all nationalities and linguistic backgrounds</li>
                                                <li><b>is supported</b> by the largest research programme of its kind</li>
                                                <li>can be taken by people with a wide range of <b>special requirements</b></li>
                                            </ul>
                                        </p>
                                        <p><b><u>Take pride in a certificate that is accepted globally</u></b></p>
                                        <p>Once you have your Cambridge English certificate, you hold one of the most valuable English qualifications in the world. It is an achievement you can be really proud of. Your Cambridge English: Key certificate will be accepted as a qualification in basic level English by thousands of leading businesses around the world, including employers like Philips, Motorola, Electrolux and Agfa-Gevaert</p>
                                     </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
            </div>
        </section><!--/about-us-->