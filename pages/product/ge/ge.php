<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="#">General English</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="about-us">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <p class="lead">General English (GE) is for you who want to improve your general English language proficiency for employment, higher education or travel purposes. GE helps students develop the full range of English language skills including reading, writing, listening, speaking, grammar, vocabulary and pronunciation. GE is made available to you at six levels from beginner to advanced level.</p>

                    <p class="lead">Upon completion of General English levels 1-4, you may choose to continue on to General English 5 and 6. Alternatively you may choose to start a <b>Mixed English and Academic Program or English for Academic Purposes</b> course to achieve an IELTS test score, TOEFL/TOEIC test score and/or gain entry to University.</p>
                    <p class="lead">
                        There are six activity-based courses/tests that give you a clear path to improve your English:
                        <ul>
                            <li>General English - Starters (Starters)</li>
                            <li>General English - Key (KET)</li>
                            <li>General English - Preliminary (PET)</li>
                            <li>General English - First (FCE)</li>
                            <li>General English - Advanced (CAE)</li>
                            <li>General English - Proficiency (CPE)</li>
                        </ul>
                    </p>
                </div>
                </div>
            </div>
        </section><!--/about-us-->
<section id="feature_other" >
        <div class="container">
           <div class="center wow fadeInDown">
                <h2></h2>
            </div>

            <div class="row">

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/general.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">General English Starters (Starters)</h3>
                            <p><a href="gestarters">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/general.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">General English Key (KET)</h3>
                            <p><a href="gekey">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/general.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">General English Preliminary (PET)</h3>
                            <p><a href="gepre">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/general.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">General English First (FCE)</h3>
                            <p><a href="gefirst">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/general.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">General English Advanced (CAE)</h3>
                            <p><a href="geadvanced">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/general.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">General English Proficiency (CPE)</h3>
                            <p><a href="gepro">know more ...</a></p>
                        </div>
                    </div>
                </div>
            </div><!--/.row--> 
        </div><!--/.container-->
    </section><!--/#feature-->