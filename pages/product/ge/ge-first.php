<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="ge">General English</a> / <a href="#">First (FCE)</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="other">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <img src="images/services/ge/first.png">
                    <p class="lead"><b><u>Ready for success in the real world</u></b><br><i>General English – First</i>, also known as <i>First Certificate in English (FCE)</i>, is an upper-intermediate level qualification. It proves you can use everyday written and spoken English for study or work purposes.</p><p>&nbsp;</p>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Why Take the course?</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">What level is <i>GE - First (FCE)</i>?</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Why take the test?</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Show you are ready for success in the real world</u></b></p>
                                                <p><i>GE – First (FCE)</i> is an upper-intermediate English language course. You can use it to help you:
                                                <ul>
                                                    <li><b>study</b> in English at foundation or pathway lev</li>
                                                    <li><b>work</b> in an English-speaking environment</li>
                                                    <li><b>live</b> independently in an English-speaking country</li>
                                                </ul>
                                                </p>

                                                <p><b><u>Gain real-life English skills for work and study</u></b></p>
                                                <p><i>GE – First (FCE)</i> shows you can use everyday written and spoken English for work and study purposes. The course uses real-life situations that are especially designed to help you:
                                                <ul>
                                                    <li><b>communicate more effectively</b></li>
                                                    <li><b>learn the language skills you need to take the next step to success</b></li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><i>GE – First (FCE)</i> is set at Level B2 of the Common European Framework of Reference for Languages (CEFR). Reaching this level means you can:
                                                <ul>
                                                    <li><b>understand the main ideas</b> of complex pieces of writing</li>
                                                    <li><b>keep up a conversation</b> on a fairly wide range of topics, expressing opinions and presenting arguments</li>
                                                    <li><b>produce clear, detailed writing,</b> expressing opinions and explaining the advantages and disadvantages of different points of view</li>
                                                </ul>
                                                Preparing for <i>GE – First (FCE)</i> will give you these kinds of practical language skills
                                                </p>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="tab-pane fade" id="tab3">
                                        <p><b><u>Take a high-quality test that is fair to everyone</u></b></p>
                                        <p><i>GE – First (FCE)</i> is respected and accepted internationally it:
                                            <ul>
                                                <li>provides the <b>most reliable</b> reflection of your language skills</li>
                                                <li>covers <b>all major varieties of English</b> (e.g. British English, American English)</li>
                                                <li>is <b>designed to be fair</b> to users of all nationalities and linguistic backgrounds</li>
                                                <li><b>is supported</b> by the largest research programme of its kind</li>
                                                <li>can be taken by people with a wide range of <b>special requirements</b></b></li>
                                            </ul>
                                        </p>
                                        <p><b><u>Take pride in a certificate that is accepted globally</u></b></p>
                                        <p>Once you have your Cambridge English certificate, you hold <b>one of the most valuable English qualifications in the world</b>. It’s an achievement you can be really proud of. Your Cambridge English: First certificate will be accepted as a qualification in upper-intermediate English by thousands of leading businesses and educational institutions around the world. They include:
                                            <ul>
                                                <li><b>employers</b> like American Express, Agfa-Gevaert, Siemens and Procter & Gamble</li>
                                                <li><b>universities</b> and colleges such as the University of Bath and the Universidad de Salamanca</li>
                                            </ul>
                                        </p>
                                     </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
            </div>
        </section><!--/about-us-->