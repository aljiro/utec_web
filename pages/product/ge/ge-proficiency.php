<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="ge">General English</a> / <a href="#">Proficiency (CPE)</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="other">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <img src="images/services/ge/proficiency.png">
                    <p class="lead"><b><u>Proof of exceptional English ability</u></b><br><i>General English – Proficiency</i>, also known as <i>Certificate of Proficiency in English (CPE)</i>, is our most advanced qualification. It proves you have achieved an extremely high level in English.</p><p>&nbsp;</p>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Why Take the course?</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">What level is <i>GE – Proficiency (CPE)</i>?</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Why take the test?</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Gain outstanding English language skills</u></b></p>
                                                <p><i>GE – Proficiency (CPE)</i> is our most advanced course. It proves that you have mastered English to an exceptional level. Use it to show you can:
                                                <ul>
                                                    <li><b>study or work at the very highest level</b> of professional and academic life</li>
                                                    <li><b>communicate with fluency and sophistication</b> similar to the level of a native speaker</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><i>GE – Proficiency (CPE)</i> is set at Level C2 of the Common European Framework of Reference for Languages (CEFR). Reaching this level means you can:
                                                <ul>
                                                    <li><b>study demanding subjects</b> at the highest level, including postgraduate and PhD programmes</b></li>
                                                    <li><b>negotiate and persuade effectively</b> at senior management level in international business settings</li>
                                                    <li><b>understand the main ideas</b> of complex pieces of writing</li>
                                                    <li><b>talk about complex or sensitive issues</b></li>
                                                    <li><b>write confidently</b> on any subject</li>
                                                </ul>
                                                Preparing for <i>GE – Proficiency (CPE)</i> will help you achieve these practical language skills
                                                </p>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="tab-pane fade" id="tab3">
                                        <p><b><u>Take a high-quality test that is fair to everyone</u></b></p>
                                        <p><i>GE – Proficiency (CPE)</i> is respected and accepted internationally it:
                                            <ul>
                                                <li>provides the <b>most reliable</b> reflection of your language skills</li>
                                                <li>covers <b>all major varieties of English</b> (e.g. British English, American English)</li>
                                                <li>is <b>designed to be fair</b> to users of all nationalities and linguistic backgrounds</li>
                                                <li><b>is supported</b> by the largest research programme of its kind</li>
                                                <li>can be taken by people with a wide range of <b>special requirements</b></li>
                                            </ul>
                                        </p>
                                        <p><b><u>Take pride in a certificate that is accepted globally</u></b></p>
                                        <p>Once you have your Cambridge English certificate, you hold <b>one of the most valuable English qualifications in the world</b>. It is an achievement you can be really proud of. Your Cambridge English: Proficiency certificate will be accepted as a qualification in near-native English by thousands of leading businesses and educational institutions around the world, including <b>employers</b> like Sony, Coca-Cola and Hewlett-Packard (HP) and <b>universities and colleges</b> such as the University of Cambridge and the University of Toronto</p>
                                     </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
            </div>
        </section><!--/about-us-->