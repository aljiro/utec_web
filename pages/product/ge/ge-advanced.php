<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="ge">General English</a> / <a href="#">Advanced (CAE)</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="other">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <img src="images/services/ge/advanced.png">
                    <p class="lead"><b><u>A badge of excellence</u></b><br>The demand for high-level English language skills is increasing all around the world. Passing <i>General English – Advanced (CAE)</i> shows that you are a high achiever</p><p>&nbsp;</p>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Why <i>GE – Advanced (CAE)</i> is a badge of excellence?</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">How students, universities and schools all benefit</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="analistic-03">How course / test takers benefit</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Accepted globally as proof of high achievement</u></b></p>
                                                <p>
                                                <ul>
                                                    <li>More than 3,000 educational institutions, businesses and government departments around the world accept <i>GE – Advanced (CAE)</i> as proof of high-level achievement</li>
                                                </ul>
                                                </p>

                                                <p><b><u>A certificate with endless opportunities</u></b></p>
                                                <p>
                                                <ul>
                                                    <li>Helps you develop the language skills you need for success and can be used for your university and student visa applications in the UK and Australia</li>
                                                </ul>
                                                </p>

                                                <p><b><u>It provides high-level English skills for academic and professional success</u></b></p>
                                                <p>
                                                <ul>
                                                    <li>Preparing for GE – Advanced (CAE) helps learners develop the skills to make the most of studying, working and living in English-speaking countries</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>A level of English for demanding study and work environments</u></b></p>
                                                <p>
                                                <ul>
                                                    <li><i>GE – Advanced (CAE)</i> shows that a student has the language ability to carry out complex research, communicate effectively at a professional level and stand out from the crowd</li>
                                                </ul>
                                                </p>

                                                <p><b><u>Worldwide availability</u></b></p>
                                                <p>
                                                <ul>
                                                    <li>There are 33 test dates every year, at 1,300 test centres in 113 countries all over the world</li>
                                                </ul>
                                                </p>

                                                <p><b><u>High-quality assessment</u></b></p>
                                                <p>
                                                <ul>
                                                    <li>From a department of the University of Cambridge</li>
                                                </ul>
                                                </p>

                                                <p><b><u>Real-life English language skills</u></b></p>
                                                <p>
                                                <ul>
                                                    <li><i>GE – Advanced (CAE)</i> offers proof of the English skills that education institutions and employers seek for high-achieving study and work situations</li>
                                                </ul>
                                                </p>

                                                <p><b>Get the English language skills which employers and universities are looking for. General English – Advanced (CAE) increases your study and employment opportunities.</b> Once you have passed your GE – Advanced (CAE) course/test, you are set for success. You can prove to universities you are prepared for study, show employers you are ready to do business and demonstrate to governments and immigration officials that you meet the language requirements for visas to enter the UK or Australia.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab3">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Gain the proof that you are a high achiever</u></b></p>
                                                <p>
                                                <ul>
                                                    <li>More than 3,000 universities, businesses, government departments and other organisations around the world recognise <i>GE – Advanced (CAE)</i></li>
                                                </ul>
                                                </p>

                                                <p><b><u>Learn excellent English for academic and professional success</u></b></p>
                                                <p>
                                                <ul>
                                                    <li>Develop the language skills you need to stand out from the crowd and make the most of studying, working and living in English-speaking environments</li>
                                                </ul>
                                                </p>

                                                <p><b><u>Get a certificate with endless opportunities</u></b></p>
                                                <p>
                                                <ul>
                                                    <li>Helps you develop the language skills you need for success and can be used for your university and student visa applications in the UK and Australia</li>
                                                </ul>
                                                </p>

                                                <p><b><u>Improve your employment opportunities</u></b></p>
                                                <p>
                                                <ul>
                                                    <li>In many countries, job applicants with strong English skills can get better jobs and earn more money than similar candidates without English skills</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div> <!--/.tab-content--> 
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
            </div>
        </section><!--/about-us-->