<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="ge">General English</a> / <a href="#">Preliminary (PET)</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="other">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <img src="images/services/ge/pre.png">
                    <p class="lead"><b><u>Practical English for everyday use</u></b><br><i>General English – Preliminary</i>, also known as Preliminary English Test (PET), is an intermediate level qualification. It shows you are able to use your English language skills for study, work  and travel.</p><p>&nbsp;</p>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Why Take the course?</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">What level is <i>GE – Key (KET)</i>?</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Why take the test?</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Prove you have practical English for everyday use</u></b></p>
                                                <p><i>GE – Preliminary (PET)</i> is an intermediate level English language course. Preparing for the test will help you improve your language skills so you can:
                                                <ul>
                                                    <li><b>understand</b> factual information</li>
                                                    <li><b>show awareness</b> of opinions, attitudes and mood in spoken and written English</li>
                                                </ul>
                                                </p>

                                                <p><b><u>Gain real-life language skills</u></b></p>
                                                <p><i>GE – Preliminary (PET)</i> shows that you can use English to communicate with native speakers for everyday purposes. The course will help you improve your language skills so you can:
                                                <ul>
                                                    <li><b>deal with everyday events</b></li>
                                                    <li><b>read simple textbooks</b> or magazine articles</li>
                                                    <li><b>write letters</b> on familiar subjects</li>
                                                    <li><b>take notes</b> in a meeting</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><i>GE – Preliminary (PET)</i> is set at Level B1 of the Common European Framework of Reference for Languages (CEFR). Reaching this level means you can:
                                                <ul>
                                                    <li><b>understand the main points</b> of straightforward instructions or public announcements</li>
                                                    <li><b>deal with most of the situations you might meet when travelling</b> as a tourist in an English-speaking country</li>
                                                    <li><b>ask simple questions</b> and take part in factual conversations in a work environment</li>
                                                    <li><b>write letters / emails</b> or make notes on familiar matters</li>
                                                </ul>
                                                Preparing for <i>GE – Preliminary (PET)</i> will give you these kinds of practical language skills
                                                </p>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="tab-pane fade" id="tab3">
                                        <p><b><u>Take a high-quality test that is fair to everyone</u></b></p>
                                        <p><i>GE – Preliminary (PET)</i> is respected and accepted internationally it:
                                            <ul>
                                                <li>provides the <b>most reliable</b> reflection of your language skills</li>
                                                <li>covers <b>all major varieties of English</b> (e.g. British English, American English)</li>
                                                <li>is <b>designed to be fair</b> to users of all nationalities and linguistic backgrounds</li>
                                                <li><b>is supported</b> by the largest research programme of its kind</li>
                                                <li>can be taken by people with a wide range of <b>special requirements</b></b></li>
                                            </ul>
                                        </p>
                                        <p><b><u>Take pride in a certificate that is accepted globally</u></b></p>
                                        <p>Once you have your Cambridge English certificate, you hold one of the most valuable English qualifications in the world. It is an achievement you can be really proud of. Your Cambridge English: Preliminary certificate will be accepted as a qualification in intermediate English by thousands of leading businesses and educational institutions around the world, including <b>employers</b> like Hewlett-Packard (HP), Sony and Coca-Cola and <b>universities and colleges</b> such as the University of Exeter and Anglia Ruskin University, Cambridge.</p>
                                     </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
            </div>
        </section><!--/about-us-->