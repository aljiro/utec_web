<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="efap">English for Academic Purposes</a> / <a href="#">IELTS</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="other">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <img src="images/services/efap/ielts.png">
                    <p class="lead"><b><u>More people go more places with IELTS</u></b><br><i>The International English Language Testing System (IELTS)</i> is the world’s most popular English test for study and migration. Each year over 1.5 million candidates take the IELTS test to start their journeys into education and employment.</p><p>&nbsp;</p><br>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Why Take the course?</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="tehnical">Why take the test?</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Prove your English language skills</u></b></p>
                                                <p>Educational institutions, employers and government immigration agencies require proof of English language skills as part of their recruitment or admission procedures. Increasingly, these organisations are choosing <i>IELTS</i></p>
                                                <p>

                                                <p><b><u>Gain worldwide recognition for study, work and immigration</u></b></p>
                                                <p><i>IELTS</i> is jointly managed by British Council, IDP: IELTS Australia and Cambridge English Language Assessment. Over 1.5 million people a year use <i>IELTS</i> to open doors to the English-speaking world and beyond. <i>IELTS</i> is recognised by more than 7,000 organisations worldwide</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Decide on a module to match your goal</u></b></p>
                                                <p>You will take a Listening, Reading, Writing and Speaking test. You can choose between two IELTS modules in the Reading and Writing tests:
                                                <ul>
                                                    <li>Academic – if you want to <b>study in English at undergraduate or postgraduate level, or seek professional registration</b></li>
                                                    <li>General Training – if you want to <b>migrate to an English-speaking country</b> (e.g. Australia, Canada, New Zealand, UK), or wish to <b>train or study in English at below degree level</b></li>
                                                </ul>
                                                All candidates take the same Listening and Speaking tests.
                                                </p>

                                                <p><b><u>Decide on a module to match your goal</u></b></p>
                                                <p>The IELTS scoring system is recognised globally, giving you <b>a truly international result</b>. You will receive a score of 0–9, with 0 being for those who did not attempt the test and 9 being for those with a high level of English</p>
                                                <p>Most <b>universities accept scores between 6–7 as being suitable for undergraduate study</b> in English. The IELTS 9-band score system is consistent. It is secure, benchmarked and understood worldwide. Test materials are designed carefully so that every version of the test is the same scale of difficulty</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
            </div>
        </section><!--/about-us-->