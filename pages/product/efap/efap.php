<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="#">English for Academic purposes</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="feature_other" >
        <div class="container">
           <div class="center wow fadeInDown">
                <h2></h2>
            </div>

            <div class="row">

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/academic.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">TOEFL</h3>
                            <p><a href="toefl">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/academic.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">TOEIC</h3>
                            <p><a href="toeic">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/academic.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">IELTS</h3>
                            <p><a href="ielts">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/academic.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">BULATS</h3>
                            <p><a href="bulats">know more ...</a></p>
                        </div>
                    </div>
                </div>
            </div><!--/.row--> 
        </div><!--/.container-->
    </section><!--/#feature-->