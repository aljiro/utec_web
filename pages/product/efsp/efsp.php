<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="#">English for Spesific purposes</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="about-us">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <p class="lead">English for Spesific Purposes (ESP) is for you who plan to study at University. The program builds on the skills learnt in General English, continues to develop to succeed at academic study. Key focus areas include listening to lectures, note-taking, planning and writing academic essays, oral presentations and group discussions.</p>
                    <p class="lead">
                        There are three activity-based courses and tests that give you a clear path to improve your English:
                        <ul>
                            <li>Business English – BEC Preliminary</li>
                            <li>Business English – BEC Vantage</li>
                            <li>Business English – BEC Higher</li>
                        </ul>
                    </p>
                    <p class="lead">Business Certificates are designed to give you practical skills to help you succeed in English-speaking international business environments.</p>
                </div>
                </div>
            </div>
        </section><!--/about-us-->
<section id="feature_other" >
        <div class="container">
           <div class="center wow fadeInDown">
                <h2></h2>
            </div>

            <div class="row">

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/spesific.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Business English – BEC Preliminary</h3>
                            <p><a href="eappre">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/spesific.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Business English – BEC Vantage</h3>
                            <p><a href="eapvan">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/spesific.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Business English – BEC Higher</h3>
                            <p><a href="eaphigh">know more ...</a></p>
                        </div>
                    </div>
                </div>
            </div><!--/.row--> 
        </div><!--/.container-->
    </section><!--/#feature-->