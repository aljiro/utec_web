<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="efsp">English for Spesific Purposes</a> / <a href="#">Vantage</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="other">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <img src="images/services/efsp/bevan.png">
                    <p class="lead"><b><u>Achieve your ambitions in international business</u></b><br><i>Business Vantage</i>, also known as <i>Business English Vantage (BEC) Vantage</i>, is the second of the three certificates. It is an intermediate level course / test.</p><p>&nbsp;</p>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Why Take the course?</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="tehnical">Why take the test?</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Achieve your ambitions in international business</u></b></p>
                                                <p><i>Business Vantage</i> is an upper-intermediate level qualification in Business English. Passing it proves to employers that you are ready for international success.</p>

                                                <p><b><u>Gain language skills for real-life business situations</u></b></p>
                                                <p><i>Business Vantage</i> is set at Level B2 of the Common European Framework of Reference for Languages (CEFR). This is the level of English required for professional and academic settings at an intermediate standard. Reaching this level shows employers you can:
                                                <ul>
                                                    <li><b>write short pieces</b> of business correspondence, reports or proposals</b></li>
                                                    <li><b>read extracts</b> from business publications</b></li>
                                                    <li><b>listen to, understand and contribute to</b> discussions in meetings</li>
                                                </ul>
                                                </p>
                                                <p>The content of <i>Business Vantage</i> reflects everyday work and business tasks. By preparing for this course, you gain skills that will be valued by employers.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Improve your employment and career prospects</u></b></p>
                                                <p>Proving your Business English abilities can open the door to <b>career opportunities</b> with a new employer or help you achieve a <b>promotion or develop your career</b>.</p>

                                                <p><b><u>Show clear evidence of your language ability</u></b></p>
                                                <p>Employers <b>value</b> and <b>rely on</b>  Business Vantage because it is a thorough course and test of all four language skills (reading, listening, writing and speaking) in a business context.</p>

                                                <p><b><u>Take a high-quality test that is fair to everyone</u></b></p>
                                                <ul>
                                                    <li><b>Detailed research and analysis</b> ensures all our tests are accurate, relevant and fair to the people that take them</li>
                                                    <li>Our tests are developed using systems and processes meeting the internationally recognised <b>ISO 9001:2008 quality management</b> standard</li>
                                                    <li>Cambridge English tests can be taken by people with a wide range of special requirements, such as specific learning, hearing or visual difficulties</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
            </div>
        </section><!--/about-us-->