<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="efsp">English for Spesific Purposes</a> / <a href="#">Higher</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="other">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <img src="images/services/efsp/behigh.png">
                    <p class="lead"><b><u>Achieve your ambitions in international business</u></b><br><i>Business Higher</i>, also known as <i>Business English Certificate (BEC) Higher</i>, is the third of the three certificates. It is a high level course / test.</p><p>&nbsp;</p>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Why Take the course?</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="tehnical">Why take the test?</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Achieve your ambitions in international business</u></b></p>
                                                <p>Passing <i>Business Higher</i> shows employers that you have the drive and motivation to achieve an advanced level of Business English, and can use your skills to function effectively in international business situations.</p>

                                                <p><i>Business Higher</i> is set at Level C1 of the Common European Framework of Reference for Languages (CEFR). This is the level of English required for demanding professional and academic settings and shows employers you can:
                                                <ul>
                                                    <li><b>communicate effectively</b> at managerial and professional level</li>
                                                    <li><b>participate with confidence</b> in workplace meetings and presentations</li>
                                                    <li><b>express yourself</b> with a high level of fluency</li>
                                                    <li><b>react appropriately</b> in different cultural and social situations</li>
                                                </ul>
                                                </p>

                                                <p>If you have not yet entered the job market, it can also be used to show universities and colleges that you can:
                                                <ul>
                                                    <li><b>study business-related subjects</b> at university level</li>
                                                    <li>carry out complex and challenging <b>research</b></li>
                                                </ul>
                                                </p>

                                                <p>The content of Business Higher reflects everyday work and business tasks. By preparing for this course, you gain skills that will be valued by employers.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><b><u>Improve your employment and career prospects</u></b></p>
                                                <p>Proving your Business English abilities can open the door to career opportunities with a new employer, helping you achieve a promotion or develop your career.</p>

                                                <p><b><u>Show clear evidence of your language ability</u></b></p>
                                                <p>Employers <b>value</b> and <b>rely on</b>  Business Higher because it is a thorough test of all four language skills (reading, listening, writing and speaking) in a business context.</p>

                                                <p><b><u>Take a high-quality course/test that is fair to everyone</u></b></p>
                                                <ul>
                                                    <li><b>Detailed research and analysis</b> ensures all our tests are accurate, relevant and fair to the people that take them</b></li>
                                                    <li>Our tests are developed using systems and processes meeting the internationally recognised <b>ISO 9001:2008 quality management</b> standard</b></li>
                                                    <li>Cambridge English tests can be taken by people with a wide range of <b>special requirements</b>, such as specific learning, hearing or visual difficulties</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
            </div>
        </section><!--/about-us-->