<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="yle">Young Learners English</a> / <a href="#">Starters</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="other">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <img src="images/services/yle/starters.png">
                    <p class="lead"><i>Young Learners English – Starters</i> is <b>the start of a child’s language learning journey</b>. The course/test introduces them to everyday written and spoken English in a fun and motivating way.</p><p class="lead"><i>YLE – Starters</i> is the first of three YLE courses/tests, aimed at children in primary and lower secondary education. </p>
                    <h2>Why take the course / test?</h2><br>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Improve children’s English one step at a time</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">Give children the confidence they need in English</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Open up a world of possibilities</a></li>
                                    <li class=""><a href="#tab4" data-toggle="tab" class="tehnical">Motivate children by rewarding their progress</a></li>
                                    <li class=""><a href="#tab5" data-toggle="tab" class="tehnical">Choose a high quality course/test that is fair to everyone</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><i>YLE – Starters</i> course / test is aimed at children in primary and lower secondary education and is designed to help them <b>build their confidence</b> when they are just starting to learn English. Children can <b>improve their English step by step</b>, by moving from Starters to Movers and to Flyers</p>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><i>YLE – Starters</i> course / test is <b>designed to motivate children</b> and give them lots of confidence to use their English. YLE – Starters course/test:
                                                <ul>
                                                    <li>
                                                        is based on familiar topics and situations
                                                    </li>
                                                    <li>
                                                        develops the skills they need to communicate in English
                                                    </li>
                                                </ul>
                                                As children develop in confidence, they will be <b>motivated to learn more English</b> and use it at a more challenging level. It gives children a <b>good foundation for language learning and shows you how they are doing in English</b>
                                                </p>
                                                <p><i>YLE – Starters</i> course / test <b>covers all four language skills</b> – reading, listening, writing and speaking</p>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="tab-pane fade" id="tab3">
                                        <p><i>YLE – Starters</i> course / test is the step towards learning <b>practical, real-life English language skills</b> that will help children:
                                            <ul>
                                                <li>
                                                    use the internet and other media in English
                                                </li>
                                                <li>
                                                    enjoy books, songs, television and films in English
                                                </li>
                                                <li>
                                                    make friends globally
                                                </li>
                                                <li>use English as the common international language</li>
                                                <li>get ready for future study and work success</li>
                                            </ul>
                                        </p>
                                     </div>
                                     
                                    <div class="tab-pane fade" id="tab4">
                                        <p>With <i>YLE – Starters</i> course / test, there is no pass or fail. <b>Every child gets an English award</b>, celebrating their achievement, building their confidence and rewarding them for developing their communication skills</p>
                                    </div>

                                    <div class="tab-pane fade" id="tab5">
                                        <p>Respected internationally, <i>YLE – Starters</i> course / test:
                                            <ul>
                                                <li>provide the most <b>reliable</b> reflection of a child’s language skills</li>
                                                <li>cover <b>all major varieties of English</b> (e.g. British English, American English)</li>
                                                <li><b>are designed</b> to <b>be fair</b> to users of all nationalities and linguistic backgrounds</li>
                                                <li><b>are supported</b> by the largest research programme of its kind</li>
                                                <li>can be taken by children with a wide range of special requirements</li>
                                            </ul>
                                        </p>
                                    </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
            </div>
        </section><!--/about-us-->