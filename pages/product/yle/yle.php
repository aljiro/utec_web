<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="product">Product</a> / <a href="#">Young Learners English</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="about-us">
        <div class="container wow fadeInDown">
            <div class="row">
                <div class="col-md-12">
                    <p class="lead"><i>Young Learners English (YLE)</i>, is a series of <b>fun, motivating English language courses / tests</b>, aimed at children in primary and lower secondary education.</p>
                    <p class="lead">
                        There are three activity-based courses/tests that give children a clear path to improve their English:
                        <ul>
                            <li>YLE – Starters</li>
                            <li>YLE – Movers</li>
                            <li>YLE – Flyers</li>
                        </ul>
                    </p>
                    <h2>Why take the course / test?</h2><br>
                    <div class="tab-wrap"> 
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Give children a head start in English</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">Develop children’s practical language skills</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Give children a positive experience of language learning</a></li>
                                    <li class=""><a href="#tab4" data-toggle="tab" class="tehnical">Reward children's progress</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><i>YLE – Movers</i> course / test is aimed at children in primary and lower secondary education and is designed to help them build their confidence when they are just starting to learn English. Children can <b>improve their English step by step</b>, by moving from <i>Movers to Flyers</i></p>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="media-body">
                                                <p><i>YLE – Movers</i> course / test is <b>designed to motivate children</b> and give them lots of confidence to use their English. <i>YLE – Movers course / test</i>:
                                                <ul>
                                                    <li>
                                                        is based on familiar topics and situations
                                                    </li>
                                                    <li>
                                                        develops the skills they need to communicate in English
                                                    </li>
                                                </ul>
                                                As children develop in confidence, they will be <b>motivated to learn more English</b> and use it at a more challenging level. It gives children a <b>good foundation for language learning and shows you how they are doing in English</b>
                                                </p>
                                                <p><i>YLE – Movers</i> course / test <b>covers all four language skills</b> – reading, listening, writing and speaking</p>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="tab-pane fade" id="tab3">
                                        <p><i>YLE – Movers</i> course / test is the step towards learning <b>practical, real-life English language skills</b> that will help children:
                                            <ul>
                                                <li>
                                                    use the internet and other media in English
                                                </li>
                                                <li>
                                                    enjoy books, songs, television and films in English
                                                </li>
                                                <li>
                                                    make friends globally
                                                </li>
                                                <li>use English as the common international language</li>
                                                <li>get ready for future study and work success</li>
                                            </ul>
                                        </p>
                                     </div>
                                     
                                    <div class="tab-pane fade" id="tab4">
                                        <p><i>With YLE – Movers</i> course / test, there is no pass or fail. <b>Every child gets an English award</b>, celebrating their achievement, building their confidence and rewarding them for developing their communication skills</p>
                                    </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->
                </div>
                </div>
            </div>
        </section><!--/about-us-->
<section id="feature_other" >
        <div class="container">
           <div class="center wow fadeInDown">
                <h2></h2>
            </div>

            <div class="row">

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/kids.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Young Learner English Starters</h3>
                            <p><a href="ylestarters">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/kids.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Young Learner English Movers</h3>
                            <p><a href="ylemovers">know more ...</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/kids.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Young Learner English Flyers</h3>
                            <p><a href="yleflyers">know more ...</a></p>
                        </div>
                    </div>
                </div>
            </div><!--/.row--> 
        </div><!--/.container-->
    </section><!--/#feature-->