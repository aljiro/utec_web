<section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media contact-info">
                        <div class="media-body">
                            You are here<h2><a href="./">Home</a> / <a href="#">Contact Us</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->
<section id="contact-info">
        <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <div class="gmap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.633663455345!2d107.00915131419315!3d-6.179764662272493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69897cb9a95439%3A0xe504c5b785628ea2!2sUtec!5e0!3m2!1sid!2sid!4v1457320877567" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li>
                                <address>
                                    <h5>Head Office of UTEC</h5>
                                    <p>Pondok Ungu Permai<br>
                                    Ruko Kavling Taman Wisata, Blok A15 No. 17-18
                                    Bahagia, Babelan, Bekasi, West Java, 17610 <br>
                                    <p>Phone: (021) 888 1919<br>
                                    Email Address: info@utec.co.id</p>
                                </address>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->